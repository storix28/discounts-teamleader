<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/products',[
    'as' => 'get_all_products',
    'uses' => 'ApiController@getAllProducts'
]);

Route::get('/customers',[
    'as' => 'get_all_customers',
    'uses' => 'ApiController@getAllCustomers'
]);

Route::get('/category',[
    'as' => 'get_all_category',
    'uses' => 'ApiController@getAllCategory'
]);

Route::post('/discount',[
    'as' => 'discount',
    'uses' => 'ApiController@getDiscountOrder'
]);
