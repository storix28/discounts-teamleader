 For this API's, the Laravel framework is used and Postman for run code.
 Json received for customers, products and orders, I have hard coded them in arrays.

 Routes: /routes/api.php
Each route consists of the method, url, alias, function name in the controller used.

 Controller: /app/Http/Controllers/ApiController.php

 Query service: / app / Services / QueryServices
Here are queries and declarations of private features that return hard-coded data.
Also in this class is called to the validation functions of the received data and the call to the class that deals with the calculation of the discount

  Validate data input: /app/CommandBus/Validators/ValidateServices.php
Following checks, functions return a json consisting of:
success - true / false, whether validated or not;
message - string, an appropriate message depending on the input;
payload - json / null;

  Discounts: /app/Services/Discounts.php
Here the discount is calculated according to certain criteria.
Api calculates a single discount in the order given in the given request.


For clone project: git clone git@bitbucket.org:storix28/discounts-teamleader.git

Get customers :
	route: /api/customers
	method: GET
Get products: 
	route: /api/products
	method: GET
Get catogoris: 
	route: /api/category
	method: GET
Get discount:
	route: /api/discount
	method: POST
	params: 
{
  "id": "3",
  "customer-id": "3",
  "items": [
    {
      "product-id": "A101",
      "quantity": "2",
      "unit-price": "9.75",
      "total": "19.50"
    },
    {
      "product-id": "A102",
      "quantity": "1",
      "unit-price": "49.50",
      "total": "49.50"
    }
  ],
  "total": "69.00"
}



	



