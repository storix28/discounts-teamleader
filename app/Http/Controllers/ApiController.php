<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 6/9/2018
 * Time: 7:14 PM
 */

namespace App\Http\Controllers;

use App\Models\ActionResult;
use App\Services\QueryService;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
class ApiController extends Controller
{
    private $queryService;

    /**
     * ApiController constructor.
     * @param QueryService $queryService
     */
    public function __construct(QueryService $queryService)
    {
        $this->queryService = $queryService;
    }

    /**
     * Get list of available products
     *
     * @return array
     */
    public function getAllProducts(){
        return $this->queryService->allProducts();
    }

    /**
     * Get list of available customers
     *
     * @return array
     */
    public function getAllCustomers(){
        return $this->queryService->allCustomers();
    }

    /**
     * Get list of product categories
     * @return array
     */
    public function getAllCategory(){
        return $this->queryService->allCategory();
    }

    /**
     * Calculate order discount based on contained products. Call discount criteria
     *
     * @param Request $request
     * @return \App\CommandBus\Infrastructure\CommandExecutionResult|ActionResult|bool|string
     */
    public function getDiscountOrder(Request $request){
        $result = $this->queryService->discountOrder($request);

        if(!$result->isSuccessful()) {
            return ActionResult::createError($result->getMessage());
        }
        return ActionResult::createSuccess($result->getMessage(), $result->getPayload());
    }
}