<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 6/9/2018
 * Time: 4:40 PM
 */

namespace App\CommandBus\Infrastructure;


/**
 * Class CommandExecutionResult
 * @package App\CommandBus\Infrastructure
 */
class CommandExecutionResult {
    protected $success;
    protected $message;
    protected $payload;

    /**
     * CommandExecutionResult constructor.
     * @param $success
     * @param $message
     * @param null $payload
     */
    private function __construct($success, $message, $payload=null) {
        $this->success = $success;
        $this->message = $message;
        $this->payload = $payload;
    }

    /**
     * @return mixed
     */
    public function isSuccessful(){
        return $this->success;
    }

    /**
     * @return mixed
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * @return null
     */
    public function getPayload(){
        return $this->payload;
    }

    /**
     * @param $message
     * @param null $payload
     * @return CommandExecutionResult
     */
    static public function Success($message, $payload=null){
        return new CommandExecutionResult(true, $message, $payload);
    }

    /**
     * @param $message
     * @param null $payload
     * @return CommandExecutionResult
     */
    static public function Error($message, $payload= null){
        return new CommandExecutionResult(false, $message, $payload);
    }
}