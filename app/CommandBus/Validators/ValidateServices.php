<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 6/9/2018
 * Time: 12:12 AM
 */

namespace App\Services;


use App\CommandBus\Infrastructure\CommandExecutionResult;

class ValidateServices
{

    /**
     * Check if all fields mandatory is set
     * @param $command
     * @return CommandExecutionResult|bool
     */
    public function checkFields($command) {
        if (!array_key_exists('id', $command)) {
            return CommandExecutionResult::Error('Check fields id!');
        }
        if (!array_key_exists('customer-id', $command)) {
            return CommandExecutionResult::Error('Check fields customer-id!');
        }
        if (!array_key_exists('total', $command)) {
            return CommandExecutionResult::Error('Check fields total!');
        }
        if (!array_key_exists('items', $command)) {
            return CommandExecutionResult::Error('Check fields items!');
        }

        foreach($command['items'] as $itemsOrder){
            if (!array_key_exists('product-id', $itemsOrder)) {
                return CommandExecutionResult::Error('Check fields product-id!');
            }
            if (!array_key_exists('quantity', $itemsOrder)) {
                return CommandExecutionResult::Error('Check fields quantity!');
            }
            if (!array_key_exists('unit-price', $itemsOrder)) {
                return CommandExecutionResult::Error('Check fields unit-price!');
            }
            if (!array_key_exists('total', $itemsOrder)) {
                return CommandExecutionResult::Error('Check fields total!');
            }
        }
        return true;
    }

    /**
     * Check if fields is not empty
     * @param $command
     * @return CommandExecutionResult|bool
     */
    public function checkFieldsType($command){
        if (!isset($command['id'])){
            return CommandExecutionResult::Error('Field id is empty!');
        }
        if (!isset($command['customer-id'])){
            return CommandExecutionResult::Error('Field customer-id is empty!');
        }
        if (!isset($command['total'])){
            return CommandExecutionResult::Error('Field total is empty!');
        }
        if (!isset($command['items'])){
            return CommandExecutionResult::Error('Field items is empty!');
        }
        foreach ($command['items'] as $itemsOrder){
            if (!isset($itemsOrder['product-id'])){
                return CommandExecutionResult::Error('Field product-id is empty!');
            }
            if (!isset($itemsOrder['quantity'])){
                return CommandExecutionResult::Error('Field quantity is empty!');
            }
            if (!isset($itemsOrder['unit-price'])){
                return CommandExecutionResult::Error('Field product-id is empty!');
            }
            if (!isset($itemsOrder['total'])){
                return CommandExecutionResult::Error('Field total is empty!');
            }
        }

        return true;
    }

    /**
     * !!! In this point, check if products exits in db by id
     * @param $command
     * @return bool
     */
    public function checkProducts($command){
        //TODO:: check if products exits in db by id
        return true;
    }
}