<?php

namespace App\Models;

use Illuminate\Http\JsonResponse;

/**
 * Class ActionResult
 * @package App\Models
 */
class ActionResult extends JsonResponse{

    /**
     * ActionResult constructor.
     * @param $isSuccessful
     * @param $message
     * @param $data
     */
    public function __construct($isSuccessful , $message , $data)
    {
        $payload = [
            'success' => $isSuccessful,
            'message' => $message,
            'data' => $data
        ];
        parent::__construct($payload, $isSuccessful ? 200 : 200);
    }

    /**
     * @param $message
     * @param null $payload
     * @return ActionResult
     */
    static public function createSuccess($message, $payload = null)
    {
        return new ActionResult(true, $message, $payload);
    }

    /**
     * @param $message
     * @param null $payload
     * @return ActionResult
     */
    static public function createError($message, $payload = null)
    {
        return new ActionResult(false, $message, $payload);
    }

}