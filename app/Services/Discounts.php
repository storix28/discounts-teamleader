<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 6/9/2018
 * Time: 4:23 PM
 */

namespace App\Services;


class Discounts {
    /**
     * Min amount for discount order
     * @var int
     */
    const MIN_AMOUNT = 1000;

    /**
     * Category id for discount: buy five, get a sixth free
     * @var int
     */
    const CATEGORY_SIX_FREE = 2;

    /**
     * Category id for discount: cheapest product
     * @var int
     */
    const CHEAPEST_PRODUCT = 1;

    /**
     * @var array
     */
    private $order = null;

    /**
     * @var string
     */
    private $message = '';

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Get discount for order
     *
     * @param $order
     * @param $allProducts
     * @return array|int|null
     */
    public function getDiscounts ($order, $allProducts){
        $this->order = $order;

        // step-by-step calling discount criteria. Currently are only three criteria available.
        if ($discount = $this->discountAmount($order)){
            return $discount;
        } else if ($discount = $this->sixthFree($order, $allProducts)){
            return $discount;
        } else if ($discount = $this->cheapestProduct($order, $allProducts)) {
            return $discount;
        }
        // call next criteria after it will be implemented
        /*
        else if (...) {
            retur ...;
        }
        */

        return 0;
    }

    /**
     * Criteria 1: check total sum products and compare it with a min amount
     *
     * @param $order
     * @return array|null
     */
    private function discountAmount($order){
        if($order['total'] > self::MIN_AMOUNT) {
            $this->message = 'You bought for over 1000 Euro. You got 10% off!';

            return $this->outputProduct($order['total']*0.1, 'Order larger than 1000');
        }

        return null;
    }

    /**
     * Criteria 2: Get sixth free from products by the same category
     *
     * @param $order
     * @param $allProducts
     * @return array|null
     */
    private function sixthFree($order, $allProducts){
        // array with product-id, value discount and details for discount
        $totalItemsDiscount = [];

        foreach ($order['items'] as $itemsOrder){
            foreach ($allProducts as $productsData){
                //check product and category
                if (($productsData['id'] === $itemsOrder['product-id'])&& ($productsData['category'] == self::CATEGORY_SIX_FREE)){
                    //check if quantity products is multiple of five and detect discount
                    $modulo = floor($itemsOrder['quantity'] / 5);
                    if ($modulo > 0) {
                        $arrayParse = $this->outputProduct($modulo*$productsData['price'], 'Sixth free', $itemsOrder['product-id']);
                        array_push($totalItemsDiscount, $arrayParse);
                    }
                }
            }
        }

        if (!empty($totalItemsDiscount)){
            $this->message = 'For five products bought from the Switches category, you got the sixth free!';
            return $totalItemsDiscount;
        }
        return null;
    }

    /**
     * Check discount for cheapest product
     * @param $order
     * @param $allProducts
     * @return null
     */
    private function cheapestProduct($order, $allProducts){
        //array with all quantity for products discount
        $allArrayQuantity= [];
        //array with all price products for discount
        $allArrayPriceProduct = [];

        //check category id and price for each product
        foreach ($order['items'] as $itemsOrder){
            foreach ($allProducts as $productsData){
                //check product and category
                if (($productsData['id'] === $itemsOrder['product-id'])&& ($productsData['category'] == self::CHEAPEST_PRODUCT)){
                    array_push($allArrayQuantity, $itemsOrder['quantity']);
                    array_push($allArrayPriceProduct, $productsData['price']);
                }
            }
        }

        //check if the quantity is greater than two and calculate discount
        if (!empty($allArrayQuantity)){
            if (array_sum($allArrayQuantity) > 2){
                $this->message = 'If the quantity is greater than two for products of Tools category, you received 20% discount from cheapest product!';

                return $this->outputProduct(round(min($allArrayPriceProduct) * 0.2, 2), 'Cheapest product');
            }
        }
        return null;
    }

    /**
     * Standardize product service result
     *
     * @param $discount
     * @param $details
     * @param null $id
     * @return array
     */
    protected function outputProduct($discount, $details, $id = null){
        return [
            'product-id' => $id,
            'discount' => $discount,
            'details' => $details
        ];
    }
}