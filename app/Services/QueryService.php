<?php
/**
 * Created by PhpStorm.
 * User=> daniel
 * Date=> 6/9/2018
 * Time=> 7=>16 PM
 */

namespace App\Services;

use App\CommandBus\Infrastructure\CommandExecutionResult;

class QueryService
{
    private $validation;
    private $discounts;

    /**
     * QueryService constructor.
     *
     * @param ValidateServices $validation
     * @param Discounts $discounts
     */
    public function __construct( ValidateServices $validation, Discounts $discounts)
    {
        $this->validation = $validation;
        $this->discounts = $discounts;
    }

    /**
     * Return all products
     *
     * @return array
     */
    public function allProducts() {
        return json_encode($this->products());
    }

    /**
     * Return all customers
     *
     * @return array
     */
    public function allCustomers() {
        return json_encode($this->customers());
    }

    /**
     * Return all categories
     *
     * @return array
     */
    public function allCategory() {
        return json_encode($this->category());
    }

    /**
     * Calculate discount value based on order info
     *
     * @param $data
     * @return \App\CommandBus\Infrastructure\CommandExecutionResult|bool|string
     */
    public function discountOrder($data){
        $command = $data->input();

        // validate order fields
        $checkFields = $this->validation->checkFields($command);
        if ($checkFields !== true) {
            return $checkFields;
        }

        // validate fields type and value
        $checkFieldsType = $this->validation->checkFieldsType($command);
        if (!$checkFieldsType) {
            return $checkFieldsType;
        }

        // check products availability
        $checkProducts = $this->validation->checkProducts($command);
        if (!$checkProducts) {
            return $checkProducts;
        }

        // retrieve al products info
        $allProducts = $this->products();

        // calculate discount - apply discounts criteria
        $getDiscount = $this->discounts->getDiscounts($command, $allProducts);

        if ($getDiscount === 0){
            return CommandExecutionResult::Error('You don\'t have discount');
        }
        return CommandExecutionResult::Success($this->discounts->getMessage(), $getDiscount);
    }


    /**
     * Static list of products. TO DO: save and read them into MySQL DB
     */
    private function products() {
        $product = [
            [
                "id" => "A101",
                "description" => "Screwdriver",
                "category" => "1",
                "price" => "9.75"
            ],
            [
                "id" => "A102",
                "description" => "Electric screwdriver",
                "category" => "1",
                "price" => "49.50"
            ],
            [
                "id" => "B101",
                "description" => "Basic on-off switch",
                "category" => "2",
                "price" => "4.99"
            ],
            [
                "id" => "B102",
                "description" => "Press button",
                "category" => "2",
                "price" => "10"
            ],
            [
                "id" => "B103",
                "description" => "Switch with motion detector",
                "category" => "2",
                "price" => "12.95"
            ]
        ];
        return $product;
    }

    /**
     * Static list of customers. Will be read from MySQL db
     *
     * @return array
     */
    private function customers() {
        $customers = [
            [
                "id"=> "1",
                "name"=> "Coca Cola",
                "since"=> "2014-06-28",
                "revenue"=> "492.12"
            ],
            [
                "id"=> "2",
                "name"=> "Teamleader",
                "since"=> "2015-01-15",
                "revenue"=> "1505.95"
            ],
            [
                "id"=> "3",
                "name"=> "Jeroen De Wit",
                "since"=> "2016-02-11",
                "revenue"=> "0.00"
            ]
        ];
        return $customers;
    }

    /**
     * Static list of categories
     *
     * @return array
     */
    private function category() {
        $category = [
            [
                'id' => 1,
                'name' => 'Tools',
            ],
            [
                'id' => 2,
                'name' => 'Switches'
            ],
        ];
        return $category;
    }
}